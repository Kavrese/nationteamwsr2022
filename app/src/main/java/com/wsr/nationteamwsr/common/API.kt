package com.wsr.nationteamwsr.common

import com.wsr.nationteamwsr.*
import com.wsr.nationteamwsr.common.models.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface API {
    @POST("login")
    fun signIn(@Body body: ModelBodyAuth): Call<ModelAnswer<ModelAuth>>

    @GET("task")
    fun tasks(@Header("Authorization") authBearer: String = "Bearer ${Info.token}"): Call<ModelAnswer<List<ModelTask>>>

    @GET("user")
    fun user(@Header("Authorization") authBearer: String = "Bearer ${Info.token}"): Call<ModelAnswer<ModelUser>>
}