package com.wsr.nationteamwsr.common

import com.wsr.nationteamwsr.common.models.ModelAnswer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Connection{
    val connectionController = ConnectionController()
}

class ConnectionController {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://sept2022.mad.hakta.pro/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val api: API = retrofit.create(API::class.java)

    companion object {
        fun <T> Call<ModelAnswer<T>>.push(onGetData: OnGetData<T>){
            enqueue(object: Callback<ModelAnswer<T>>{
                override fun onResponse(
                    call: Call<ModelAnswer<T>>,
                    response: Response<ModelAnswer<T>>,
                ) {
                    if (response.body() != null){
                        val content = response.body()!!
                        if (content.success){
                            onGetData.onGet(content.data)
                        }else{
                            onGetData.onError(content.message)
                        }
                    }else{
                        if (response.code() == 401)
                            onGetData.onError("Unauthorised")
                        else
                            onGetData.onError("Body null")
                    }
                }

                override fun onFailure(call: Call<ModelAnswer<T>>, t: Throwable) {
                    onGetData.onError(t.localizedMessage ?: t.message ?: "Unknown error")
                }

            })
        }
    }
}

interface OnGetData <T>{
    fun onGet(data: T)
    fun onError(message: String)
}