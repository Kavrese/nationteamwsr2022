package com.wsr.nationteamwsr.common.models

data class ModelUser(
    val avatar: String,
    val id: Int,
    val name: String,
    val email: String
)