package com.wsr.nationteamwsr.common.models

data class ModelBodyAuth (
    val email: String,
    val password: String
)