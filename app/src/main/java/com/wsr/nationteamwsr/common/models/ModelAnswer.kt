package com.wsr.nationteamwsr.common.models

data class ModelAnswer <T>(
    val success: Boolean,
    val data: T,
    val message: String
)