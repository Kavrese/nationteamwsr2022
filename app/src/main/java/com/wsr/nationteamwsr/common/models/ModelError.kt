package com.wsr.nationteamwsr.common.models

data class ModelError (
    val error: String
)