package com.wsr.nationteamwsr.common.models

data class ModelTask (
    val id: Int,
    val title: String,
    val deadline: String,
    val estimated_duration: Int,
    val actual_duration: Int,
    val status: ModelStatus
)

data class ModelStatus(
    val title: String
)