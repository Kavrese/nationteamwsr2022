package com.wsr.nationteamwsr.common.models

data class ModelAuth(
    val token: String,
    val name: String
)
