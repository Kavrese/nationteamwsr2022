package com.wsr.nationteamwsr.screens.TasksScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.wsr.nationteamwsr.R
import com.wsr.nationteamwsr.TypeTasks
import com.wsr.nationteamwsr.TypesTaskAdapter
import com.wsr.nationteamwsr.common.Connection
import com.wsr.nationteamwsr.common.ConnectionController.Companion.push
import com.wsr.nationteamwsr.common.OnGetData
import com.wsr.nationteamwsr.common.models.ModelTask
import com.wsr.nationteamwsr.common.models.ModelUser
import com.wsr.nationteamwsr.screens.SignInScreen.showAlertError
import kotlinx.android.synthetic.main.activity_tasks.*

class TasksActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tasks)

        Connection.connectionController.api.tasks().push(object: OnGetData<List<ModelTask>> {
            override fun onGet(data: List<ModelTask>) {
                rec_types.apply {
                    val allTypes = listOf("In progress", "New", "Finished")
                    val types = allTypes.map { title ->
                        TypeTasks(title, data.filter { it.status.title == title })
                    }
                    adapter = TypesTaskAdapter(types)
                    layoutManager = LinearLayoutManager(this@TasksActivity)
                }
            }

            override fun onError(message: String) {
                showAlertError(this@TasksActivity, message)
            }
        })

        Connection.connectionController.api.user().push(object: OnGetData<ModelUser> {
            override fun onGet(data: ModelUser) {
                Glide.with(this@TasksActivity)
                    .load("https://sept2022.mad.hakta.pro/storage/${data.avatar}")
                    .into(avatar)
            }

            override fun onError(message: String) {
                showAlertError(this@TasksActivity, message)
            }
        })
    }
}