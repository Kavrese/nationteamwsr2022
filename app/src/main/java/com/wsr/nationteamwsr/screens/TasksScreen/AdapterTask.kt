package com.wsr.nationteamwsr.screens.TasksScreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.wsr.nationteamwsr.R
import com.wsr.nationteamwsr.SimpleViewHolder
import com.wsr.nationteamwsr.common.models.ModelTask
import kotlinx.android.synthetic.main.task.view.*

class AdapterTask(private val tasks: List<ModelTask>): RecyclerView.Adapter<SimpleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.task, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        val model = tasks[position]
        val colorStoke = when(model.status.title){
            "In progress" -> R.color.red
            "New" -> R.color.yellow
            "Finished" -> R.color.green
            else -> R.color.black
        }
        val drawableType = when(model.status.title){
            "In progress" -> R.drawable.progress
            "New" -> R.drawable.new_img
            "Finished" -> R.drawable.finished
            else -> R.drawable.progress
        }

        val textDetails = when(model.status.title){
            "In progress" -> "Not completed yet"
            "New" -> "Not completed yet"
            "Finished" -> "Completed in 128 minutes"
            else -> ""
        }

        holder.itemView.card_type.strokeColor =
            ContextCompat.getColor(holder.itemView.context, colorStoke)
        holder.itemView.status_img.setImageDrawable(ContextCompat.getDrawable(holder.itemView.context, drawableType))

        holder.itemView.title_task.text = model.title
        holder.itemView.time_text.text = model.estimated_duration.toString() + " min."
        holder.itemView.deadline_text.text = model.deadline
        holder.itemView.details_time.text = textDetails

        if (model.estimated_duration < model.actual_duration && model.status.title == "Finished"){
            holder.itemView.gr.visibility = View.VISIBLE
            holder.itemView.time_text.setTextColor(ContextCompat.getColor(holder.itemView.context,
                R.color.red))
        }else{
            holder.itemView.gr.visibility = View.INVISIBLE
            holder.itemView.time_text.setTextColor(ContextCompat.getColor(holder.itemView.context,
                R.color.black))
        }
    }

    override fun getItemCount(): Int = tasks.size
}