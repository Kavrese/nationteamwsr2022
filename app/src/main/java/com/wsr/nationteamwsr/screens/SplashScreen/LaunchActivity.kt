package com.wsr.nationteamwsr.screens.SplashScreen

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.constraintlayout.motion.widget.MotionLayout
import com.wsr.nationteamwsr.R
import com.wsr.nationteamwsr.common.Info
import com.wsr.nationteamwsr.screens.SignInScreen.SignInActivity
import com.wsr.nationteamwsr.screens.TasksScreen.TasksActivity
import kotlinx.android.synthetic.main.activity_launch.*

class LaunchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        val sh = getSharedPreferences("app", Context.MODE_PRIVATE)

        Handler(Looper.getMainLooper()).postDelayed({
            motion.transitionToEnd()
        }, 300)

        motion.setTransitionListener(object: MotionLayout.TransitionListener{
            override fun onTransitionStarted(
                motionLayout: MotionLayout?,
                startId: Int,
                endId: Int,
            ) {}

            override fun onTransitionChange(
                motionLayout: MotionLayout?,
                startId: Int,
                endId: Int,
                progress: Float,
            ) {}

            override fun onTransitionCompleted(motionLayout: MotionLayout?, currentId: Int) {
                Handler(Looper.getMainLooper()).postDelayed({
                    if (sh.checkTokenExist()) {
                        Info.token = sh.getString("token", "")!!
                        startActivity(Intent(this@LaunchActivity, TasksActivity::class.java))
                    }else
                        startActivity(Intent(this@LaunchActivity, SignInActivity::class.java))
                    finish()
                }, 1900)
            }

            override fun onTransitionTrigger(
                motionLayout: MotionLayout?,
                triggerId: Int,
                positive: Boolean,
                progress: Float,
            ) {}

        })
    }
}

fun SharedPreferences.saveToken(){
    edit()
        .putString("token", Info.token)
        .apply()
}

fun SharedPreferences.checkTokenExist(): Boolean{
    return getString("token", "") != ""
}