package com.wsr.nationteamwsr

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.wsr.nationteamwsr.common.models.ModelTask
import com.wsr.nationteamwsr.screens.TasksScreen.AdapterTask
import kotlinx.android.synthetic.main.item_type.view.*

class SimpleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

class TypesTaskAdapter(private var types: List<TypeTasks> = listOf()): RecyclerView.Adapter<SimpleViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_type, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        val model = types[position]
        val titleStatus = when(model.status){
            "Finished" -> "Completed"
            "In progress" -> "In Progress"
            else -> model.status
        }

        holder.itemView.title_type.text = titleStatus

        holder.itemView.rec_tasks.adapter = AdapterTask(model.tasks)
        if (model.status == "Finished"){
            holder.itemView.rec_tasks.layoutManager = LinearLayoutManager(holder.itemView.context)
        }else{
            holder.itemView.rec_tasks.layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
        }
    }

    override fun getItemCount(): Int = types.size
}

data class TypeTasks(
    val status: String,
    val tasks: List<ModelTask>
)