package com.wsr.nationteamwsr.screens.SignInScreen

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.wsr.nationteamwsr.R
import com.wsr.nationteamwsr.common.Connection
import com.wsr.nationteamwsr.common.ConnectionController.Companion.push
import com.wsr.nationteamwsr.common.Info
import com.wsr.nationteamwsr.common.OnGetData
import com.wsr.nationteamwsr.common.models.ModelAuth
import com.wsr.nationteamwsr.common.models.ModelBodyAuth
import com.wsr.nationteamwsr.screens.SplashScreen.saveToken
import com.wsr.nationteamwsr.screens.TasksScreen.TasksActivity
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        auth.setOnClickListener {
            val email = login.text.toString()
            val password = password.text.toString()

            if (email.isEmpty()){
                showAlertError(this, "Email is empty !")
                return@setOnClickListener
            }

            if (password.isEmpty()){
                showAlertError(this, "Password is empty !")
                return@setOnClickListener
            }

            Connection.connectionController.api.signIn(ModelBodyAuth(email, password)).push(object:
                OnGetData<ModelAuth> {
                override fun onGet(data: ModelAuth) {
                    Info.token = data.token
                    getSharedPreferences("app", Context.MODE_PRIVATE).saveToken()
                    startActivity(Intent(this@SignInActivity, TasksActivity::class.java))
                    finish()
                }

                override fun onError(message: String) {
                    showAlertError(this@SignInActivity, message)
                }
            })
        }
    }
}

fun showAlertError(context: Context, error: String){
    AlertDialog.Builder(context)
        .setTitle("Error !")
        .setMessage(error)
        .setNegativeButton("OK", null)
        .show()
}